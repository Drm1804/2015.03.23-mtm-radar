//спойлер Логистики

function labelSpoil(obj){
    var labelText = obj.getElementsByClassName('b-renta-color__label');
    var textForLabel = obj.getElementsByClassName('b-renta-color__dop-label');
    if(obj.getAttribute('data-id') == 1){
        obj.setAttribute('data-id', '2');
        labelText[0].className += ' b-rc-check';
        textForLabel[0].style.display = 'none';
        textForLabel[1].style.display = 'block';
    }else{
        obj.setAttribute('data-id', '1');
        labelText[0].classList.remove('b-rc-check');
        textForLabel[0].style.display = 'block';
        textForLabel[1].style.display = 'none';
    }
}
function menuStepTwo(){
    var menuTwo = document.getElementsByClassName('b-header__row--step2');
    if (menuTwo[0].style.display != 'block'){
        menuTwo[0].style.display = 'block';
    }else{
        menuTwo[0].style.display = "none";
    }
}